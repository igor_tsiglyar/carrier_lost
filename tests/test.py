#!/usr/bin/env python

import os
import sys
from itertools import product
from subprocess import call, Popen, PIPE
from threading import Thread
from random import shuffle
from time import sleep


def has_carrier(link):
    answer = Popen(['ip', 'link', 'show', link], stdout=PIPE)
    answer.wait()
    return 'NO-CARRIER' not in answer.communicate()[0]


def is_up(link):
    answer = Popen(['ip', 'link', 'show', link], stdout=PIPE)
    answer.wait()
    return 'UP' in answer.communicate()[0]


def change_link_state(link, desired_state):
    if desired_state != is_up(link):
        command = 'ifup ' if desired_state else 'ifdown ' 
        call(command + link + ' > /dev/null', shell=True)


def change_carrier_state(link, desired_state):
    if desired_state != has_carrier(link):
        attr_file = '/sys/kernel/carrier_control/' + link + '/carrier'
        if os.path.isfile(attr_file):
            open(attr_file, 'w+').write('1' if desired_state else '0')


def wait(link, desired_carrier_state):
    while is_up(link) and has_carrier(link) != desired_carrier_state:
        pass


def check_correctness(link, iter_num):
    if is_up(link):
        bad = 0
        desired_state = False
        for i in xrange(2 * iter_num):
            change_carrier_state(link, desired_state)
            wait(link, desired_state)
            desired_state = not desired_state
            bad = bad + 1 if desired_state else bad - 1
            sleep(.35)
        return not bad


def check_stability(link, iter_num):
    for i in xrange(iter_num):
        desired_states_list = list(product([True, False], repeat=4))
        shuffle(desired_states_list)
        for desired_states in desired_states_list:
            change_carrier_state(link, desired_states[0])
            wait(link, desired_states[0])
            change_link_state(link, desired_states[1])
            change_carrier_state(link, desired_states[2])
            wait(link, desired_states[2])
            change_link_state(link, desired_states[3])


if __name__ == '__main__':
    link = sys.argv[1]
    iter_num = int(sys.argv[2])
    if check_correctness(link, iter_num):
        print 'correctness test passed'
    else:
        print 'correctness test failed'
    check_stability(link, iter_num)
    print 'stablity test passed'
    change_link_state(link, True)
    change_carrier_state(link, True)
