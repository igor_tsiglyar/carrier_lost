#!/usr/bin/env python

import re
import sys
from subprocess import Popen, PIPE


class Monitor:


    def __init__(self, link):
        self.link = link
        addrs = Popen(['ip', 'addr', 'show', link], stdout=PIPE).communicate()[0]
        self.is_up = 'UP' in addrs
        self.has_carrier = 'NO-CARRIER' not in addrs if self.is_up else 'unknown'
        self.MAC = re.search('link/ether (?P<MAC>.*?) ', addrs).group('MAC')
        self.IPv4 = re.findall('inet (?P<IP>.*?) ', addrs)
        self.IPv6 = re.findall('inet6 (?P<IP>.*?) ', addrs)
        all_routes = Popen(['ip', 'route'], stdout=PIPE).communicate()[0].split('\n')
        self.routes = filter(lambda x: 'dev ' + link + ' ' in x, all_routes)


    def check_interface_state(self):
        interface_state = Popen(['ip', 'link', 'show', self.link], stdout=PIPE).communicate()[0]
        is_up = 'UP' in interface_state
        if is_up != self.is_up:
            self.is_up = is_up
            print self.link + ' is up' if self.is_up else self.link + ' is down'


    def check_carrier_state(self):
        interface_state = Popen(['ip', 'link', 'show', self.link], stdout=PIPE).communicate()[0]
        has_carrier = 'NO-CARRIER' not in interface_state if self.is_up else 'unknown'
        if has_carrier != self.has_carrier:
            self.has_carrier = has_carrier
            if self.has_carrier == 'unknown':
                print self.link + ': carrier state is unknown'
            else:
                print self.link + ' has carrier' if self.has_carrier else self.link + ' has no carrier'


    def check_MAC(self):
        interface_state = Popen(['ip', 'link', 'show', self.link], stdout=PIPE).communicate()[0]
        MAC = re.search('link/ether (?P<MAC>.*?) ', interface_state).group('MAC')
        if MAC != self.MAC:
            self.MAC = MAC
            print self.link + ' MAC address changed to ' + self.MAC


    def check_IPv4(self):
        addrs = Popen(['ip', 'addr', 'show', self.link], stdout=PIPE).communicate()[0]
        IPv4 = re.findall('inet (?P<IP>.*?) ', addrs)
        if IPv4 != self.IPv4:
            old_IPv4, new_IPv4 = set(self.IPv4), set(IPv4)
            for addr in new_IPv4 - old_IPv4:
                print self.link + ': IPv4 address ' + addr + ' added'
            for addr in old_IPv4 - new_IPv4:
                print self.link + ': IPv4 address ' + addr + ' deleted' 
            self.IPv4 = IPv4


    def check_IPv6(self):
        addrs = Popen(['ip', 'addr', 'show', self.link], stdout=PIPE).communicate()[0]
        IPv6 = re.findall('inet6 (?P<IP>.*?) ', addrs)
        if (IPv6 != self.IPv6):
            old_IPv6, new_IPv6 = set(self.IPv6), set(IPv6)
            for addr in new_IPv6 - old_IPv6:
                print self.link + ': IPv6 address ' + addr + ' added'
            for addr in old_IPv6 - new_IPv6:
                print self.link + ': IPv6 address ' + addr + ' deleted'
            self.IPv6 = IPv6


    def check_routes(self):
        all_routes = Popen(['ip', 'route'], stdout=PIPE).communicate()[0].split('\n')
        routes = filter(lambda x: 'dev ' + self.link + ' ' in x, all_routes)
        if (routes != self.routes):
            old_routes, new_routes = set(self.routes), set(routes)
            for route in new_routes - old_routes:
                print self.link + ': route ' + route + ' added'
            for route in old_routes - new_routes:
                print self.link + ': route ' + route + ' deleted'
            self.routes = routes


    def track_changes(self):
        self.check_interface_state()
        self.check_carrier_state()
        self.check_MAC()
        self.check_IPv4()
        self.check_IPv6()
        self.check_routes()


if __name__ == '__main__':
    monitors = [Monitor(link) for link in sys.argv[1:]]
    while open('monitoring').read()[:-1] == '1':
        map(Monitor.track_changes, monitors)
