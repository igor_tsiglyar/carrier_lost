#!/usr/bin/env python

import re
import os
import sys
import time
from subprocess import call, Popen, PIPE
from random import randint, choice, shuffle
from string import hexdigits
from monitor import Monitor


class Changer:


    def __init__(self, link):
        self.link = link
        addrs = Popen(['ip', 'addr', 'show', link], stdout=PIPE).communicate()[0]
        self.is_up = 'UP' in addrs
        self.is_carrier = 'NO-CARRIER' not in addrs if self.is_up else 'unknown'
        self.MAC = re.search('link/ether (?P<MAC>.*?) ', addrs).group('MAC')
        self.IPv4, self.IPv6, self.routes = [], [], []


    def change_link_state(self):
        if self.is_up:
            call('ip link set ' + self.link + ' down', shell=True)
            self.IPv4, self.IPv6, self.routes = [], [], []
        else:
            call('ip link set ' + self.link + ' up', shell=True)
        self.is_up = not self.is_up


    def change_carrier_state(self):
        attr_file = '/sys/kernel/carrier_control/' + self.link + '/turn_carrier_on'
        if os.path.isfile(attr_file):
            attr = open(attr_file, 'w+')
            attr.write('0' if self.carrier_on else '1')
            self.carrier_on = not self.carrier_on


    def change_MAC(self):
        self.MAC = self.MAC[::-1]
        if self.is_up:
            self.change_link_state()
            call('ip link set dev ' + self.link + ' addr ' + self.MAC, shell=True)
            self.change_link_state()
        else:
            call('ip link set dev ' + self.link + ' addr ' + self.MAC, shell=True)


    def add_IPv4(self):
        addr = '192.168.0.' + str(randint(0, 255)) + '/24'
        if addr not in self.IPv4:
            call('ip addr add ' + addr + ' dev ' + self.link, shell=True)
            self.IPv4.append(addr)


    def del_IPv4(self):
        if self.IPv4:
            addr = choice(self.IPv4)
            call('ip addr del ' + addr + ' dev ' + self.link, shell=True)
            self.IPv4.remove(addr)


    def add_IPv6(self):
        addr = reduce(lambda res, x: res + choice(hexdigits[:-6]), xrange(4), '::') + '/64'
        if addr not in self.IPv6:
            call('ip addr add ' + addr + ' dev ' + self.link, shell=True)
            self.IPv6.append(addr)


    def del_IPv6(self):
        if self.IPv6:
            addr = choice(self.IPv6)
            call('ip addr del ' + addr + ' dev ' + self.link, shell=True)
            self.IPv6.remove(addr)


    def add_route(self):
        route = '192.168.' + str(randint(0, 255)) + '.0/24'
        if route not in self.routes:
            call('ip route add ' + route + ' dev ' + self.link, shell=True)
            self.routes.append(route)


    def del_route(self):
        if self.routes:
            route = choice(self.routes)
            call('ip route del ' + route + ' dev ' + self.link, shell=True)
            self.routes.remove(route)


if __name__ == '__main__':
    monitors = [Monitor(link) for link in sys.argv[1:-1]]
    changers = [Changer(link) for link in sys.argv[1:-1]]
    delay = float(sys.argv[-1])
    while open('changing').read()[:-1] == '1':
        for changer, monitor in zip(changers, monitors):
            changer.change_MAC()
            changer.add_IPv4()
            changer.add_IPv6()
            changer.add_route()
            changer.change_carrier_state()
            monitor.track_changes()
            changer.change_MAC()
            changer.del_IPv4()
            changer.del_IPv6()
            changer.del_route()
            changer.change_carrier_state()
            monitor.track_changes()
        time.sleep(delay)
