#include "sysfs_cc.h"
#include "cc.h"
#include <linux/netdevice.h>

ssize_t carrier_show(struct tport *port, struct port_attribute *attr, char *buf)
{
    return sprintf(buf, "%d\n", netif_carrier_ok(port->netdev));
}


ssize_t carrier_store(struct tport *port, struct port_attribute *attr, const char *buf, size_t count)
{
    sscanf(buf, "%d", &port->has_carrier);
    if (port->has_carrier == 1) {
        port->carrier_on(port);
    } else if (port->has_carrier == 0) {
        port->carrier_off(port);
    }
    return count;
}


ssize_t port_attr_show(struct kobject *kobj, struct attribute *attr, char *buf)
{
    struct port_attribute *attribute = container_of(attr, struct port_attribute, attr);
    struct tport *port = container_of(kobj, struct tport, kobj);

    if (!attribute->show) {
        return -EIO;
    }

    return attribute->show(port, attribute, buf);
}


ssize_t port_attr_store(struct kobject *kobj, struct attribute *attr, const char *buf, size_t count)
{
    struct port_attribute *attribute = container_of(attr, struct port_attribute, attr);
    struct tport *port = container_of(kobj, struct tport, kobj);

    if (!attribute->store) {
        return -EIO;
    }

    return attribute->store(port, attribute, buf, count);
}


void port_release(struct kobject *kobj)
{
    int i;
    struct tport *port = container_of(kobj, struct tport, kobj);
    for (i = 0; i < port->irq_count && port->dev_name[i]; ++i) {
        kfree(port->dev_name[i]);
    }
    kfree(port);
}

extern void update_interfaces(void);

ssize_t update_interfaces_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    update_interfaces();
    return sprintf(buf, "%s", "");
}
