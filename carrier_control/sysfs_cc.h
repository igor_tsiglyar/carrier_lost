#include <linux/sysfs.h>

#ifndef _SYSFS_CC_H
#define _SYSFS_CC_H

struct tport;
struct kobj_attribute;

struct port_attribute {
    struct attribute attr;
    ssize_t (*show)(struct tport *, struct port_attribute *, char *);
    ssize_t (*store)(struct tport *, struct port_attribute *, const char *, size_t);
};

ssize_t carrier_show(struct tport *port, struct port_attribute *attr, char *buf);

ssize_t carrier_store(struct tport *port, struct port_attribute *attr, const char *buf, size_t count);

ssize_t port_attr_show(struct kobject *kobj, struct attribute *attr, char *buf);

ssize_t port_attr_store(struct kobject *kobj, struct attribute *attr, const char *buf, size_t count);

void port_release(struct kobject *kobj);

ssize_t update_interfaces_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);

#endif