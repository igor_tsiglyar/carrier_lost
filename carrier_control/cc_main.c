#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/kallsyms.h>
#include "tg3_cc.h"
#include "bnx2x_cc.h"
#include "e1000_cc.h"
#include "sysfs_cc.h"
#include "cc.h"


struct irq_desc* (*irq_to_desc_)(unsigned int);
const char* (*netdev_drivername_)(const struct net_device*);


bool default_save_irq_context(struct tport *port)
{
    int i;
    struct irqaction* action;
    struct net_device* dev = port->netdev;

    for (i = 0; i < port->irq_count; ++i) {
        struct irq_desc *desc = irq_to_desc_(port->irq[i]);
        bool found = false;
        
        for (action = desc->action; action != NULL; action = action->next) {
            if (action->dev_id == dev) {
                found = true;
            }
            if (found) { 
                port->handler[i] = action->handler;
                port->flags[i] = action->flags;
                port->dev_id[i] = action->dev_id;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}


bool load_irq_context(struct tport* port)
{
    int i;
    for (i = 0; i < port->irq_count; ++i) {
        if (request_irq(port->irq[i], port->handler[i], port->flags[i],
                        port->dev_name[i], port->dev_id[i])) {
            return false;
        }
    }
    return true;
}


void default_carrier_on(struct tport *port)
{
    struct net_device *dev = port->netdev;
    if (!netif_carrier_ok(dev) && load_irq_context(port)) { 
        netif_carrier_on(dev);
    }
}


void default_carrier_off(struct tport *port)
{
    int i;
    struct net_device *dev = port->netdev;
    if (netif_carrier_ok(dev) && port->save_irq_context(port)) {
        for (i = 0; i < port->irq_count; ++i) {
            disable_irq(port->irq[i]);
            free_irq(port->irq[i], port->dev_id[i]); 
            enable_irq(port->irq[i]);
        }
        netif_carrier_off(dev);
    }
}


static struct sysfs_ops port_ops = {
    .show = port_attr_show,
    .store = port_attr_store,
};


static struct port_attribute carrier_attribute =
    __ATTR(carrier, S_IWUSR | S_IRWXO, carrier_show, carrier_store);


static struct attribute *port_default_attrs[] = {
    &carrier_attribute.attr,
    NULL,
};


static struct kobj_type port_ktype = {
    .sysfs_ops = &port_ops,
    .release = port_release,
    .default_attrs = port_default_attrs,
};


static struct kset *ports;
static struct tport port_list;


void find_dev_irq (struct tport *port)
{
    struct irq_desc *desc;
    unsigned int irq, i = 0;
    for (irq = 0; irq < NR_IRQS; ++irq) {
        desc = irq_to_desc_(irq);
        if (desc && desc->dir && desc->dir->subdir) {
            if (strstr(desc->dir->subdir->name, port->netdev->name)) {
               port->dev_name[i] = kzalloc(strlen(desc->dir->subdir->name) + 1, GFP_KERNEL);
               strcpy(port->dev_name[i], desc->dir->subdir->name);
               port->irq[i++] = irq;
            }
        }
    }
    port->irq_count = i;
}


// initializes function pointers with driver-specific functions
// or default implementations
void set_callbacks(struct tport *port)
{
    const char* drivername = netdev_drivername_(port->netdev);
    if (strcmp(drivername, "bnx2x") == 0) {
        port->save_irq_context = bnx2x_save_irq_context;
        port->carrier_off = default_carrier_off;
        port->carrier_on = default_carrier_on;
    } else if (strcmp(drivername, "tg3") == 0) {
        port->save_irq_context = tg3_save_irq_context;
        port->carrier_off = tg3_carrier_off;
        port->carrier_on = tg3_carrier_on;
    } else if (strcmp(drivername, "e1000") == 0) {
        port->save_irq_context = default_save_irq_context;
        port->carrier_off = e1000_carrier_off;
        port->carrier_on = e1000_carrier_on;
        port->extra_init = set_delayed_work;
    } else {
        port->save_irq_context = default_save_irq_context;
        port->carrier_off = default_carrier_off;
        port->carrier_on = default_carrier_on;
    }
}


struct tport* create_one_port(struct net_device *netdev)
{
    struct tport *port = kzalloc(sizeof(struct tport), GFP_KERNEL);

    if (!port) {
        return NULL;
    }

    port->kobj.kset = ports;
    port->netdev = netdev;
    port->has_carrier = netif_carrier_ok(netdev);
    find_dev_irq(port);
    set_callbacks(port);
    if (port->extra_init) {
        port->extra_init(port);
    }
    
    INIT_LIST_HEAD(&port->list);

    if (kobject_init_and_add(&port->kobj, &port_ktype, NULL, "%s", netdev->name)) {
        kobject_put(&port->kobj);
        return NULL;
    }

    kobject_uevent(&port->kobj, KOBJ_ADD);
    return port;
}


void destroy_port_list(void)
{
    if (!list_empty(&port_list.list)) {
        struct tport *port, *tmp;
        list_for_each_entry_safe(port, tmp, &port_list.list, list) {
        list_del(&port->list);
            kobject_put(&port->kobj);
        }
        INIT_LIST_HEAD(&port_list.list);
    }
}


void update_interfaces(void)
{
    struct net_device *dev;
    bool in_list;
    struct tport tmp_port_list, *port;
    INIT_LIST_HEAD(&tmp_port_list.list);
    read_lock(&dev_base_lock);
    for_each_netdev(&init_net, dev) {
        if (strcmp(dev->name, "lo") && strcmp(netdev_drivername_(dev), "")) {
            in_list = false;
            list_for_each_entry(port, &port_list.list, list) {
                if (port->netdev == dev) {
                    list_move_tail(&port->list, &tmp_port_list.list);
                    in_list = true;
                    break;
                }
            }
            if (!in_list) {
                port = create_one_port(dev);
                if (port) {
                    list_add_tail(&port->list, &tmp_port_list.list);
                }
            }
        }
    }
    read_unlock(&dev_base_lock);
    if (tmp_port_list.list.next) {
        destroy_port_list();
        list_replace(&tmp_port_list.list, &port_list.list);
    }
}


static struct kobj_attribute update_interfaces_attribute =
    __ATTR(update_interfaces, S_IRUSR | S_IROTH, update_interfaces_show, NULL);


static int __init carrier_control_init(void) {
    unsigned long symbol;
    struct net_device *dev;
    struct tport *port;

    if ((symbol = kallsyms_lookup_name("irq_to_desc")) == 0) {
        return -EINVAL;
    }
    irq_to_desc_ = (struct irq_desc* (*)(unsigned int)) symbol;

    if ((symbol = kallsyms_lookup_name("netdev_drivername")) == 0) {
        return -EINVAL;
    }
    netdev_drivername_ = (const char* (*)(const struct net_device*)) symbol;

    ports = kset_create_and_add("carrier_control", NULL, kernel_kobj);
    if (!ports) {
        return -ENOMEM;
    }

    if(sysfs_create_file(&ports->kobj, &update_interfaces_attribute.attr)) {
        kset_unregister(ports);
        return -EINVAL;
    }

    INIT_LIST_HEAD(&port_list.list);
    read_lock(&dev_base_lock);
    for_each_netdev(&init_net, dev) {
        if (strcmp(dev->name, "lo") && strcmp(netdev_drivername_(dev), "")) {
            port = create_one_port(dev);
            if (!port) {            
                sysfs_remove_file(&ports->kobj, &update_interfaces_attribute.attr);
                destroy_port_list(); 
                kset_unregister(ports);
                read_unlock(&dev_base_lock);
                return -ENOMEM;
            }
            list_add_tail(&port->list, &port_list.list);
        }
    }
    read_unlock(&dev_base_lock);
    return 0;
}


static void __exit carrier_control_exit(void) {
    struct tport *port;
    list_for_each_entry(port, &port_list.list, list) {
        port->carrier_on(port);
    }
    sysfs_remove_file(&ports->kobj, &update_interfaces_attribute.attr);
    destroy_port_list(); 
    kset_unregister(ports);
}


module_init(carrier_control_init);
module_exit(carrier_control_exit);
MODULE_LICENSE("GPL");
