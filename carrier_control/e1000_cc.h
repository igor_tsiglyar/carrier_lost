#ifndef _E1000_CC
#define _E1000_CC

struct tport;

void set_delayed_work(struct tport *port);

void e1000_carrier_off(struct tport *port);

void e1000_carrier_on(struct tport* port);

#endif