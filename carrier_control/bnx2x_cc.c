#include "../drivers/net/ethernet/broadcom/bnx2x/bnx2x.h"
#include "bnx2x_cc.h"
#include "cc.h"

bool bnx2x_save_irq_context(struct tport* port)
{
    int i, j;
    struct irqaction* action;
    struct net_device* dev = port->netdev;

    for (i = 0; i < port->irq_count; ++i) {
        struct irq_desc *desc = irq_to_desc_(port->irq[i]);
        bool found = false;
        
        for (action = desc->action; action != NULL; action = action->next) {
            if (action->dev_id == dev) {
                found = true;
            } else {
                struct bnx2x* bp = netdev_priv(dev);
                struct bnx2x_fastpath* fp = bnx2x_ooo_fp(bp);

                if (action->dev_id == fp) {
                    found = true;
                } else {
                    for_each_eth_queue(bp, j) {
                        fp = &bp->fp[j];
                        if (action->dev_id == fp) {
                            found = true;
                        }                
                    }
                }
            }
            if (found) { 
                port->handler[i] = action->handler;
                port->flags[i] = action->flags;
                port->dev_id[i] = action->dev_id;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}
