#include <linux/kobject.h>
#include <linux/interrupt.h>

#ifndef _CC_H
#define _CC_H

#define max_irq_count 5

struct net_device;

struct tport {
    struct kobject kobj;
    struct net_device *netdev;
    int has_carrier;
    int irq_count;
    int irq[max_irq_count];
    irq_handler_t handler[max_irq_count];
    unsigned int flags[max_irq_count];
    void* dev_id[max_irq_count];
    char* dev_name[max_irq_count];
    struct list_head list;
    // callbacks to set actual driver implementations
    bool (*save_irq_context)(struct tport*);
    void (*carrier_off)(struct tport*);
    void (*carrier_on)(struct tport*);
    // driver specific information
    void* extra;
    void (*extra_init)(struct tport*);
};

struct irq_desc;

extern struct irq_desc* (*irq_to_desc_)(unsigned int);

extern const char* (*netdev_drivername_)(const struct net_device*);

bool default_save_irq_context(struct tport *port);

bool load_irq_context(struct tport* port);

void default_carrier_on(struct tport *port);

void default_carrier_off(struct tport *port);

void find_dev_irq(struct tport *port);

void set_callbacks(struct tport *port);

void update_interfaces(void);

struct tport* create_one_port(struct net_device *netdev);

#endif
