#ifndef _BNX2X_CC_H
#define _BNX2X_CC_H

struct tport;

bool bnx2x_save_irq_context(struct tport* port);

#endif
