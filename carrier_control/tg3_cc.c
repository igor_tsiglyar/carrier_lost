#include "cc.h"
#include "tg3_cc.h"
#include <linux/netdevice.h>

// all this includes and defines is here only to include tg3.h without errors
#include <linux/timecompare.h>
#include <linux/version.h>
#include <linux/clocksource.h>
#include <linux/ptp_clock_kernel.h>

#ifndef PCI_D0
#define PCI_D0
#endif

#ifndef IS_ENABLED
#define __ARG_PLACEHOLDER_1 0,
#define config_enabled(cfg) _config_enabled(cfg)
#define _config_enabled(value) __config_enabled(__ARG_PLACEHOLDER_##value)
#define __config_enabled(arg1_or_junk) ___config_enabled(arg1_or_junk 1, 0)
#define ___config_enabled(__ignored, val, ...) val
#define IS_ENABLED(option) \
        (config_enabled(option) || config_enabled(option##_MODULE))
#endif

#include "../drivers/net/ethernet/broadcom/tg3_flags.h"
#include "../drivers/net/ethernet/broadcom/tg3_firmware.h"
#include "../drivers/net/ethernet/broadcom/tg3.h"


bool tg3_save_irq_context(struct tport* port)
{
    int i, irq_num = 0;
    struct irqaction* action;
    struct net_device* dev = port->netdev;

    for (i = 0; i < port->irq_count; ++i) {
        struct irq_desc *desc = irq_to_desc_(port->irq[i]);
        bool found = false;
        
        for (action = desc->action; action != NULL; action = action->next) {
            if (action->dev_id == dev) {
                found = true;
            } else {
                struct tg3* tp = netdev_priv(dev);
                struct tg3_napi* tnapi = &tp->napi[irq_num++];
                if (action->dev_id == tnapi) {
                    found = true;
                }
            }
            if (found) { 
                port->handler[i] = action->handler;
                port->flags[i] = action->flags;
                port->dev_id[i] = action->dev_id;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}


void tg3_carrier_off(struct tport* port)
{
    int i;
    struct tg3* tp = netdev_priv(port->netdev);
    struct net_device *dev = port->netdev;
    if (netif_carrier_ok(dev) && port->save_irq_context(port)) {
        for (i = 0; i < port->irq_count; ++i) {
            disable_irq(port->irq[i]);
            free_irq(port->irq[i], port->dev_id[i]); 
            enable_irq(port->irq[i]);
        }
        // we disable timer that calles interrupt handler while interrupts are disabled
        del_timer_sync(&tp->timer);
        netif_carrier_off(dev);
    }
}


void tg3_carrier_on(struct tport* port)
{
    struct tg3* tp = netdev_priv(port->netdev);
    struct net_device *dev = port->netdev;
    if (!netif_carrier_ok(dev) && load_irq_context(port)) {
        // after enabling interrupts we enable timer as well
        tp->asf_counter = tp->asf_multiplier;
        tp->timer_counter = tp->timer_multiplier;
        tp->timer.expires = jiffies + tp->timer_offset;
        add_timer(&tp->timer);
        netif_carrier_on(dev);
    }
}
