#ifndef _TG3_CC_H
#define _TG3_CC_H

struct tport;

bool tg3_save_irq_context(struct tport* port);

void tg3_carrier_off(struct tport* port);

void tg3_carrier_on(struct tport* port);

#endif