#include "../drivers/net/ethernet/intel/e1000/e1000.h"
#include "e1000_cc.h"
#include "cc.h"

// obtain watchdog that syncronizes system carrier state
// and actual physical state
void set_delayed_work(struct tport *port)
{
    struct net_device *dev = port->netdev;
    struct e1000_adapter *private = netdev_priv(dev);
    struct delayed_work *delayed = &private->watchdog_task;
    if (delayed) {
        port->extra = delayed;
    }
}


void e1000_carrier_off(struct tport *port)
{
    int i;
    struct net_device *dev = port->netdev;
    struct delayed_work* delayed = port->extra;
    if (netif_carrier_ok(dev) && port->save_irq_context(port)) {
        for (i = 0; i < port->irq_count; ++i) {
            disable_irq(port->irq[i]);
            free_irq(port->irq[i], port->dev_id[i]); 
            enable_irq(port->irq[i]);
        }
        // switch off watchdog
        // otherwise it will turn carrier on again
        cancel_delayed_work_sync(delayed);     
        netif_carrier_off(dev);
    }
}


void e1000_carrier_on(struct tport* port)
{
    struct net_device *dev = port->netdev;
    struct delayed_work* delayed = port->extra;
    if (!netif_carrier_ok(dev) && load_irq_context(port)) { 
        netif_carrier_on(dev);
        // restore watchdog
        INIT_DELAYED_WORK(delayed, delayed->work.func);
    }
}

